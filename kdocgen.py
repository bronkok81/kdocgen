#!/usr/bin/python3

"""
Script for KiCad documentation automation
Based on kicad_7_doc_gen.py by Jordan Aceto
https://github.com/JordanAceto/kicad_7_cli_doc_gen

Rich Holmes / March 2023
"""

import os
import argparse
import fnmatch
import json
from copy import deepcopy
from KiCadProj import KiCadProj

def find_kdirs(pattern: str, path: str) -> list[str]:
    """`find_kdirss(pattern, path)` returns paths containing files matching pattern `pattern`."""
    ret = []
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                ret.append(root)
    return ret

def merge_params (ps1: dict[str], ps2: dict[str]) -> dict[str] :
    """`merge_params (ps1, ps2)` updates parameter structure ps1 with ps2 and returns it"""
    ps1a = deepcopy(ps1)
    ps2a = deepcopy(ps2)
    for entry in ps2a:
        if entry not in ps1a:
            print (f"***** Unknown configuration file entry '{entry}' ignored")
            continue
        if entry == "ibom" or entry == "pcb_svg" or entry == "pcb_pdf" or entry == "gerbers":
            for entry2 in ps2a[entry]:
                if entry2 not in ps1a[entry]:
                    print (f"***** Unknown configuration file entry '{entry2}' in '{entry}' ignored")
                    continue
                ps1a[entry][entry2] = ps2a[entry][entry2]
        else:
            ps1a[entry] = ps2a[entry]
    return ps1a

def sch_do (k_proj: KiCadProj) -> None:
    """Process sch dependent stuff"""
    k_proj.schem_pdf()
    k_proj.bom()

def pcb_do (k_proj: KiCadProj) -> None:
    """Process PCB dependent stuff, `sep` if doing separation."""
    k_proj.pcb_2D_layers()
    k_proj.ibom()
    k_proj.gerbers()
    k_proj.step()
    return

def readme_start (proj_dir: str, proj_name: str) -> str:
    """Returns starting content for a basic README, if no README file already exists, else "" """
    readme_exists = any([os.path.isfile(f"{proj_dir}/README.md"),
                         os.path.isfile(f"{proj_dir}/Readme.md"),
                         os.path.isfile(f"{proj_dir}/readme.md")])
    if not readme_exists:
        print("*** Generating basic README file")
        return f"# {proj_name}\n\n"
    else:
        print("*** README already exists, skipping README")
        return ""

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'project_dir',
        type=str,
        help='path to directory encompassing one or more KiCad projects'
    )
    parser.add_argument(
        '-v', '--version',
        action='version',
        version="%(prog)s v0.2.0"
    )
    parser.add_argument(
        '-p', '--params',
        action='store_true',
        help='show parameters for each KiCad project and exit'
    )

    args = parser.parse_args()

    proj_name = os.path.basename(os.path.realpath(args.project_dir))
    proj_dir = args.project_dir.strip("/ ")
    if not os.path.isdir (proj_dir):
        print (f"*** Project directory not found: {proj_dir}")
        return
    kproj_dir_list = find_kdirs("*.kicad_pro", proj_dir)
    params = {
        "enabled": ["schem_pdf", "ibom", "bom", "pcb_svg", "pcb_pdf", "gerbers", "step", "readme"],
        "docs_dir": "docs",
        "2d_dir": "2D",
        "3d_dir": "3D",
        "bom_dir": "BOM",
        "gerb_dir": "Gerbers",
        "kikit_path": "/usr/local/bin/kikit",
        "separation": [],
        "ibom": {
            "path": "/usr/local/bin/generate_interactive_bom.py",
            "dark_mode": True
        },
        "bom": {
            "path": "/usr/share/kicad/plugins/bom_csv_grouped_extra.py",
            "args" : "",
            "ext" : "csv"
        },
        "pcb_svg": {
            "front_layers": ["Edge.Cuts","F.SilkS","F.Cu"],
            "back_layers": ["Edge.Cuts","B.SilkS","B.Cu"],
            "theme": "",
            "black_and_white": False,
            "page_size_mode": 2,
            "exclude_drawing_sheet": True
        },
        "pcb_pdf": {
            "front_layers": ["F.Cu", "In1.Cu", "In2.Cu", "F.SilkS", "F.Paste", "F.Mask"],
            "back_layers": ["B.Cu", "B.SilkS", "B.Paste", "B.Mask"],
            "edge_layer": "Edge.Cuts",
            "exclude_refdes": True,
            "exclude_value": True,
            "include_border_title": True,
            "theme": "",
            "black_and_white": True
        },
        "gerbers": {
            "kikit": False,
            "compress": 2,
            "layers": ["F.Cu", "In1.Cu", "In2.Cu", "B.Cu", "F.SilkS", "B.SilkS", "F.Paste", "B.Paste", "F.Mask", "B.Mask", "Edge.Cuts"],
            "exclude_refdes": False,
            "exclude_value": False,
            "include_border_title": False,
            "no-protel-ext": False,
            "separate": [],
            "sepname": [],
            "fabhouse": "jlcpcb",
            "drc": True,
            "options": ""
        }
    }

    # Update params from global and semilocal rc files

    home_directory = os.path.expanduser( '~' )
    global_rc_path = os.path.join (home_directory, '.kdocgenrc.json')
    semilocal_rc_path = os.path.join (proj_dir, '.kdocgenrc.json')

    for rc_path in [global_rc_path, semilocal_rc_path]:
        if os.path.isfile (rc_path):
            with open (rc_path, "r") as f:
                try:
                    params_override = json.load (f)
                except Exception as e:
                    print (f"*** Failed to load {rc_path}")
                    print (e)
                    exit()
                params = merge_params (params, params_override)
    
    readme_content = readme_start (proj_dir, proj_name) if "readme" in params["enabled"] else ""
    doreadme = readme_content != ""
    
    # Process each k-project

    for kproj_dir in kproj_dir_list:
        print (f"*** Processing {kproj_dir}")
        kparams = dict(params)
        if kproj_dir != proj_dir:
            local_rc_path = os.path.join (kproj_dir, '.kdocgenrc.json')
            if os.path.isfile (local_rc_path):
                with open (local_rc_path, "r") as f:
                    params_override = json.load (f)
                    kparams = merge_params (kparams, params_override)

        if args.params:
            print (json.dumps(kparams, indent=4))
            continue

        with KiCadProj(proj_dir, proj_name, kproj_dir, kparams) as k_proj:

            # generate the outputs

            sch_do (k_proj)

            if len(k_proj.params["separation"]) == 0:
                pcb_do(k_proj)
                if doreadme:
                    readme_content += k_proj.readme()
            else:
                for isep in range(len(k_proj.params["separation"])):
                    k_proj.next_separation()
                    pcb_do(k_proj)
                    if doreadme:
                        readme_content += k_proj.readme()

    if doreadme:
        with open(f"{proj_dir}/README.md", "w") as f:
            f.write (readme_content)

if __name__ == '__main__':
    main()
